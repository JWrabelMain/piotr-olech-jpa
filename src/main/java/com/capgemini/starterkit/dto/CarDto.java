package com.capgemini.starterkit.dto;

public class CarDto extends CreateUpdateCarDto {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarDto{" +
                "id=" + id +
                ", carType=" + carType +
                ", category='" + category + '\'' +
                ", productionYear=" + productionYear +
                ", color='" + color + '\'' +
                ", horsepower=" + horsepower +
                ", capacity=" + capacity +
                ", mileage=" + mileage +
                ", carWatchersIds=" + carWatchersIds +
                '}';
    }
}
