package com.capgemini.starterkit.dto;

import com.capgemini.starterkit.entity.CarType;

import java.math.BigDecimal;
import java.util.List;

public class CreateUpdateCarDto {
    protected CarType carType;
    protected String category;
    protected Integer productionYear;
    protected String color;
    protected Integer horsepower;
    protected BigDecimal capacity;
    protected BigDecimal mileage;
    protected List<Integer> carWatchersIds;

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Integer productionYear) {
        this.productionYear = productionYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(Integer horsepower) {
        this.horsepower = horsepower;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public List<Integer> getCarWatchersIds() {
        return carWatchersIds;
    }

    public void setCarWatchersIds(List<Integer> carWatchersIds) {
        this.carWatchersIds = carWatchersIds;
    }
}
