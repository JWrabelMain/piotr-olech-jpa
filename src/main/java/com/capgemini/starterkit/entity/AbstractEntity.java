package com.capgemini.starterkit.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditListener.class)
public class AbstractEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "creation_time")
    @CreatedDate
    private Date creationTime;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "modification_time")
    @LastModifiedDate
    private Date modificationTime;

    @Version
    private int version;

    private String persistingHistory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(Date modificationTime) {
        this.modificationTime = modificationTime;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getPersistingHistory() {
        return persistingHistory;
    }

    public void setPersistingHistory(String persistingHistory) {
        this.persistingHistory = persistingHistory;
    }
}
