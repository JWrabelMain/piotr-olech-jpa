package com.capgemini.starterkit.entity;

import javax.persistence.PrePersist;
import java.time.LocalTime;

public class AuditListener {

    @PrePersist
    public void ammendPersistingHistory(AbstractEntity entity) {
        String persistingHistory = entity.getPersistingHistory();

        if(persistingHistory == null) {
            persistingHistory="";
        }
        persistingHistory = persistingHistory + "| Persisted: " + LocalTime.now() +" |";
        entity.setPersistingHistory(persistingHistory);
    }

}
