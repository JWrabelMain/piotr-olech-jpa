package com.capgemini.starterkit.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "cars")
public class Car extends AbstractEntity {
    @ManyToOne
    private CarType carType;
    @Size(max = 30)
    private String category;
    @Column(name = "production_year")
    private Integer productionYear;
    @Size(max = 20)
    private String color;
    private Integer horsepower;
    private BigDecimal capacity;
    private BigDecimal mileage;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "car")
    private List<Rent> rents;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "carwatchers",
            joinColumns = @JoinColumn(name = "car_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"))
    private List<Employee> carWatchers;

    @Override
    public String toString() {
        return "Car{" +
                "carType=" + carType +
                ", category='" + category + '\'' +
                ", productionYear=" + productionYear +
                ", color='" + color + '\'' +
                ", horsepower=" + horsepower +
                ", capacity=" + capacity +
                ", mileage=" + mileage +
                ", rents=" + rents +
                ", carWatchers: " + carWatchers.size() +
                '}';
    }

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }

    public List<Employee> getCarWatchers() {
        return carWatchers;
    }

    public void setCarWatchers(List<Employee> carWatchers) {
        this.carWatchers = carWatchers;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Integer productionYear) {
        this.productionYear = productionYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(Integer horsepower) {
        this.horsepower = horsepower;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

}
