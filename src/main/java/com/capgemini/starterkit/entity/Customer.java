package com.capgemini.starterkit.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

@Entity
@Table(name = "customers")
public class Customer extends AbstractEntity {


    @ManyToOne
    private Address address;


    @Size(max = 30)
    @NotNull
    @Column(name = "first_name")
    private String firstName;


    @Size(max = 50)
    @NotNull
    @Column(name = "last_name")
    private String lastName;


    @Size(max = 15)
    @NotNull
    @Column(name = "phone_number")
    private String phoneNumber;

    @Size(max = 50)
    @NotNull
    private String email;


    @Size(max = 16, min = 16)
    @NotNull
    @Column(name="credit_card",columnDefinition="char(16)")
    private String creditCart;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreditCart() {
        return creditCart;
    }

    public void setCreditCart(String creditCart) {
        this.creditCart = creditCart;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @NotNull
    @Column(name = "date_of_birth")
    private Date dateOfBirth;


}
