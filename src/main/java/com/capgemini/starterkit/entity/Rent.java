package com.capgemini.starterkit.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "rents")
public class Rent extends AbstractEntity {
    @ManyToOne
    private Car car;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Branch branchEnd;
    @ManyToOne
    private Branch branchStart;

    private BigDecimal payment;

    @NotNull
    @Column(name = "start_time", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "end_time", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Branch getBranchEnd() {
        return branchEnd;
    }

    public void setBranchEnd(Branch branchEnd) {
        this.branchEnd = branchEnd;
    }

    public Branch getBranchStart() {
        return branchStart;
    }

    public void setBranchStart(Branch branchStart) {
        this.branchStart = branchStart;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
