package com.capgemini.starterkit.repository;

import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.dto.CreateUpdateCarDto;
import com.capgemini.starterkit.entity.Car;
import com.capgemini.starterkit.entity.CarType;

import java.util.List;

public interface CarRepository {

    Car findById(Integer carId);
    void save(Car car);

    CarDto addCar(CreateUpdateCarDto carDto);

    void updateCar(Integer carId, CreateUpdateCarDto carDto);

    void deleteCar(Integer carId);

    void assignWatcher(Integer carId, Integer employeeId);

    List<CarDto> findByType(CarType carType);

    List<CarDto> findByBrand(String brand);
}
