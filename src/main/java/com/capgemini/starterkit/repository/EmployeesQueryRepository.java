package com.capgemini.starterkit.repository;

import com.capgemini.starterkit.entity.Employee;

import java.util.List;

public interface EmployeesQueryRepository {
    List<Employee> find(EmployeeSearchCriteria searchCriteria);
}
