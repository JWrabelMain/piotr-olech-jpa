package com.capgemini.starterkit.repository.impl;

import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.dto.CreateUpdateCarDto;
import com.capgemini.starterkit.entity.Car;
import com.capgemini.starterkit.entity.CarType;
import com.capgemini.starterkit.entity.Employee;
import com.capgemini.starterkit.repository.CarRepository;
import com.capgemini.starterkit.repository.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private EmployeesRepository employeesRepository;

    @Override
    public Car findById(Integer carId) {
        return entityManager.createQuery("SELECT c from Car c where c.id = :id", Car.class)
                .setParameter("id", carId)
                .getSingleResult();
    }

    @Override
    public void save(Car car) {
        entityManager.persist(car);
    }

    @Override
    public CarDto addCar(CreateUpdateCarDto carDto) {
        Car carEntity = transformDtoToEntity(carDto);
        entityManager.persist(carEntity);
        return transformEntityToDto(carEntity);
    }

    @Override
    public void updateCar(Integer carId, CreateUpdateCarDto carDto) {
        Car car = findById(carId);
        updateCarFromDto(carDto, car);
        entityManager.persist(car);
    }

    @Override
    public void deleteCar(Integer carId) {
        entityManager.remove(findById(carId));
    }

    @Override
    public void assignWatcher(Integer carId, Integer employeeId) {
        Car car = findById(carId);
        Employee employee = employeesRepository.findOne(employeeId);

        car.getCarWatchers().add(employee);

        entityManager.persist(car);
    }

    @Override
    public List<CarDto> findByType(CarType carType) {
        List<Car> matchingCars = entityManager.createQuery("SELECT c from Car c where c.carType = :carType", Car.class)
                .setParameter("carType", carType)
                .getResultList();

        return transformEntitiesToDtosList(matchingCars);
    }


    @Override
    public List<CarDto> findByBrand(String brand) {
        List<Car> matchingCars = entityManager.createQuery("SELECT c from Car c where c.carType.brand = :brand", Car.class)
                .setParameter("brand", brand)
                .getResultList();

        return transformEntitiesToDtosList(matchingCars);
    }

    private List<CarDto> transformEntitiesToDtosList(List<Car> matchingCars) {
        List<CarDto> carDtos = new ArrayList<>();

        for (Car car : matchingCars) {
            carDtos.add(transformEntityToDto(car));
        }
        return carDtos;
    }


    private CarDto transformEntityToDto(Car car) {
        CarDto carDto = new CarDto();

        carDto.setId(car.getId());
        carDto.setCapacity(car.getCapacity());
        carDto.setCarType(car.getCarType());
        carDto.setCategory(car.getCategory());
        carDto.setColor(car.getColor());
        carDto.setHorsepower(car.getHorsepower());
        carDto.setMileage(car.getMileage());
        carDto.setProductionYear(car.getProductionYear());

        List<Integer> carWatchersIds = new ArrayList<>();
        for (Employee employee : car.getCarWatchers()) {
            carWatchersIds.add(employee.getId());
        }
        carDto.setCarWatchersIds(carWatchersIds);

        return carDto;
    }


    private Car transformDtoToEntity(CreateUpdateCarDto carDto) {
        Car car = new Car();
        updateCarFromDto(carDto, car);

        return car;
    }

    private void updateCarFromDto(CreateUpdateCarDto carDto, Car car) {
        car.setCapacity(carDto.getCapacity());
        car.setCarType(carDto.getCarType());
        car.setCategory(carDto.getCategory());
        car.setColor(carDto.getColor());
        car.setHorsepower(carDto.getHorsepower());
        car.setMileage(carDto.getMileage());
        car.setProductionYear(carDto.getProductionYear());

        List<Employee> carWatchers = new ArrayList<>();
        for (Integer employeeId : carDto.getCarWatchersIds()) {
            carWatchers.add(employeesRepository.findOne(employeeId));
        }
        car.setCarWatchers(carWatchers);
    }


}
