package com.capgemini.starterkit.repository.impl;

import com.capgemini.starterkit.entity.Employee;
import com.capgemini.starterkit.repository.EmployeeSearchCriteria;
import com.capgemini.starterkit.repository.EmployeesQueryRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class EmployeesQueryRepositoryImpl implements EmployeesQueryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Employee> find(EmployeeSearchCriteria searchCriteria) {


        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Employee> query = cb.createQuery(Employee.class);

        Root<Employee> employeeRoot = query.from(Employee.class);

        query.select(employeeRoot);

        if (searchCriteria.getBranchId() != null) {
            query.where(cb.equal(employeeRoot.get("branch").get("id"), searchCriteria.getBranchId()));
        }

        if (searchCriteria.getEmployeePosition() != null) {
            query.where(cb.equal(employeeRoot.get("position"), searchCriteria.getEmployeePosition()));
        }

        if (searchCriteria.getCarId() != null) {
            query.where(cb.equal(employeeRoot.get("watchedCars").get("id"), searchCriteria.getCarId()));
        }




        return entityManager.createQuery(query).getResultList();
    }
}
