package com.capgemini.starterkit.rest;

import com.capgemini.starterkit.entity.CarType;
import com.capgemini.starterkit.entity.Employee;
import com.capgemini.starterkit.entity.EmployeePosition;
import com.capgemini.starterkit.repository.CarRepository;
import com.capgemini.starterkit.repository.EmployeeSearchCriteria;
import com.capgemini.starterkit.repository.EmployeesQueryRepository;
import com.capgemini.starterkit.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.starterkit.service.BookService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@RestController
public class TestRestController {

	@Autowired
	private CarRepository carService;
	@Autowired
	private EmployeesQueryRepository employeesQueryRepository;

	@RequestMapping("/test")
	public String runForTest() {
		return carService.findByBrand("Ford").toString();
	}

	@RequestMapping("/testQuery")
	public String testQuery() {
		return employeesQueryRepository.find(new EmployeeSearchCriteria(1, null, null)).toString();
	}

}
