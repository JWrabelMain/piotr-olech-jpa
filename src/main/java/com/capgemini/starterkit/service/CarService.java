package com.capgemini.starterkit.service;

import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.dto.CreateUpdateCarDto;
import com.capgemini.starterkit.entity.CarType;

import java.util.List;

public interface CarService {
    CarDto addCar(CreateUpdateCarDto carDto);

    void updateCar(Integer carId, CreateUpdateCarDto carDto);

    void deleteCar(Integer carId);

    void assignWatcher(Integer carId, Integer employeeId);

    List<CarDto> findByType(CarType carType);

    List<CarDto> findByBrand(String brand);
}
