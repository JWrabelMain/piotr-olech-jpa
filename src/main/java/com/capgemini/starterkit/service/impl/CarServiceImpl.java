package com.capgemini.starterkit.service.impl;

import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.dto.CreateUpdateCarDto;
import com.capgemini.starterkit.entity.CarType;
import com.capgemini.starterkit.repository.CarRepository;
import com.capgemini.starterkit.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;


    @Override
    public CarDto addCar(CreateUpdateCarDto carDto) {
        return carRepository.addCar(carDto);
    }

    @Override
    public void updateCar(Integer carId, CreateUpdateCarDto carDto) {
        carRepository.updateCar(carId, carDto);
    }

    @Override
    public void deleteCar(Integer carId) {
        carRepository.deleteCar(carId);
    }

    @Override
    public void assignWatcher(Integer carId, Integer employeeId) {
        carRepository.assignWatcher(carId, employeeId);
    }

    @Override
    public List<CarDto> findByType(CarType carType) {
        return carRepository.findByType(carType);
    }

    @Override
    public List<CarDto> findByBrand(String brand) {
        return carRepository.findByBrand(brand);
    }
}
